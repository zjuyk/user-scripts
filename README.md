# User scripts

My useful scripts

- [addshadow](.local/bin/addshadow)

![addshadow](screenshots/addshadow.png)

add shadow, round corner, border and watermark to clipboard image.

- [videowallpaper](.local/bin/videowallpaper)

![videowallpaper](screenshots/wallpaper.gif)

Use xwinwrap and mpv to implement video wallpaper

- [lock.sh](.local/bin/lock.sh)

![lock.sh](screenshots/lock.png)

add covers, avatar, time and weather to i3lock

# License
MIT
