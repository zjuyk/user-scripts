#
# Author: 千玄子 (zjuyk)
# Date:   3 Jan, 2021
# Powershell v5.1
#

# add execution policy
set-ExecutionPolicy Unrestricted

# get image from clipboard
$img = Get-Clipboard -Format Image
if ($img -eq $null) {
    Write-Host "Clipboard contains no image."
    Exit
}
$img.Save("C:\tmp\src.png")

# add round corner
$Parameters_1

# send image to clipboard
Get-Content "C:\tmp\des.png" | Set-Clipboard

# [Reflection.Assembly]::LoadWithPartialName('System.Drawing');
# [Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms');

# $filename = 'C:\Users\Administrator\Pictures\background-logo-1024.png';
# $file = get-item($filename);
# $img = [System.Drawing.Image]::Fromfile($file);
# [System.Windows.Forms.Clipboard]::SetImage($img);
# $img.Dispose()

# make a done notification
Write-Host "done!"


